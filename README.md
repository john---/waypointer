Waypointer
==========
Application to manage waypoints

Dependencies
------------

* Mojolicious
* Postgres
* Modules/Plugins
  * Mojo::Pg
