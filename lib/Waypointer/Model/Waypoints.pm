package Waypointer::Model::Waypoints;
use Mojo::Base -base;

use warnings;
use strict;

use DateTime;

use Carp ();
use Data::Dumper;

has postgres => sub { Carp::croak 'db is required' };
has log      => sub { Carp::croak 'log is required' };
has config   => sub { Carp::croak 'config is required' };

sub get {
    my $self = shift;

    my $urls = $self
	->postgres
	->db
	->query('select * from waypoints order by entered desc')
	->hashes;

    return $urls;
};

sub save {
    my ($self, $hash) = @_;

    my $id;
    if ($hash->{id}) {
        $id = $self
    	    ->postgres
	    ->db
            ->query('update waypoints set x = ?, y = ?, z = ?, label = ?, description = ?, world = ? where id = ?',
                     $hash->{x}, $hash->{y}, $hash->{z},
                     $hash->{label}, $hash->{description}, $hash->{world}, $hash->{id});
        $id = $hash->{id};
    } else {
        $id = $self
    	    ->postgres
	    ->db
	    ->query(
                'insert into waypoints (x, y, z, label, description, world) values (?, ?, ?, ?, ?, ?) returning id',
                 $hash->{x}, $hash->{y}, $hash->{z},
                 $hash->{label}, $hash->{description}, $hash->{world}
                   )->hash->{id};
    }
    $self->log->debug("from logurl id: " . $id);

    return { id => $id };
};

sub delete {
    my ($self, $id) = @_;

    my $cnt = $self
	->postgres
	->db
	->query('delete from waypoints where id = ?', $id
               );

    $self->log->debug("delete waypoint id: $id");

    # TODO:  some error handling to address failed deletes
    return { id => $id };
}

1;
