package Waypointer::Controller::Main;

use Mojo::Base 'Mojolicious::Controller';
use Data::Dumper;

# This action will render a template
sub waypoints {
    my $self = shift;

    $self->render;
}

sub items {
    my $self = shift;

    my $urls = $self->model->get();
    #my $urls = $self->model->get_waypoints( { search => $self->param('search') } );

    $self->render( json =>  { data => $urls->to_array } );
}

sub save {
    my $self = shift;

    #print "in controller id: " . $self->param('id') . "\n";
    $self->req->params->to_hash;
    
    my $resp;
    $resp = $self->model->save( $self->req->params->to_hash);

    $self->render(json => $resp);
}

sub delete {
    my $self = shift;

    my $resp = $self->model->delete( $self->param('id') );

    $self->render(json => $resp);
}

1;
