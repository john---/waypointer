package Waypointer;
use Mojo::Base 'Mojolicious';

use Mojo::Pg;

use Waypointer::Model::Waypoints;

# This method will run once at server start
sub startup {
    my $self = shift;

    # Load configuration from hash returned by "my_app.conf"
    my $config = $self->plugin('Config');

    $self->secrets( $self->config('secrets') );

    $self->helper( postgres => sub { state $pg = Mojo::Pg->new( $config->{pg} ) } );

    $self->helper(model => sub {
	my $c = shift;
	return Waypointer::Model::Waypoints->new(
	    postgres => $self->postgres,
	    log      => $c->app->log,
	    config   => $self->config,
	);
    });

    # Router
    my $r = $self->routes;

    # Normal route to controller
    $r->get('/')->to('main#waypoints');
    $r->get('/items')->to('main#items');   # handles the ajax for populating bookmarks
    $r->post('/save')->to('main#save');
    $r->post('/delete')->to('main#delete');
}

1;
