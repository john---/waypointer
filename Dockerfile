FROM perl:latest

WORKDIR /opt/app

#COPY . .  # this is needed only for deploying self-contained container
COPY cpanfile .
RUN cpanm --installdeps -n .

CMD [ "perl", "/usr/local/bin/morbo", "./script/waypointer", "daemon" ]
#CMD [ "perl", "./script/waypointer", "daemon" ]
